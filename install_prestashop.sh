#! /bin/bash
sudo apt-get update
sudo apt-get upgrade -y

#Install all of the required modules for apache2 and prestashop
sudo apt install -y apache2 libapache2-mod-php mysql-server 
sudo apt install software-properties-common
sudo add-apt-repository -y ppa:ondrej/php
sudo apt update
sudo apt upgrade -y
sudo apt install -y php7.4
sudo apt install -y php7.4-cli php7.4-common php7.4-curl php7.4-zip php7.4-gd php7.4-mysql php7.4-xml php7.4-mbstring php7.4-json php7.4-intl
sudo apt update
sudo apt upgrade -y
sudo a2enmod rewrite

#Modify apache2 default configuration
mem_limit='128M'
max_upload_filesize='32M'
sudo sed -i "s/^memory_limit =.*/memory_limit = ${mem_limit}/" /etc/php/7.4/cli/php.ini
sudo sed -i "s/^upload_max_filesize =.*/upload_max_filesize = ${max_upload_filesize}/" /etc/php/7.4/cli/php.ini
sudo systemctl restart apache2

#Install mysql-client
sudo apt install mysql-client-core-8.0

#Create a connection profile to connect to the remote mysql-database
sudo touch /etc/mysql/connection_properties.cnf
sudo chmod 400 /etc/mysql/connection_properties.cnf
cat > /etc/mysql/connection_properties.cnf << EOF
[client]
host=${ps_db_arn}
user=${ps_db_username}
password=${ps_db_password}
EOF

#Connect to the remote database and create Prestashop Database/User
mysql --defaults-extra-file=/etc/mysql/connection_properties.cnf <<MYSQL_SCRIPT
create user 'prestashop'@'%' identified by 'admin123';
CREATE DATABASE prestashop;
grant all privileges on prestashop.* to 'prestashop'@'%';
flush privileges;
quit
MYSQL_SCRIPT

#Download and install prestashop
cd /opt 
sudo wget https://download.prestashop.com/download/releases/prestashop_1.7.6.9.zip
sudo apt install unzip
sudo unzip prestashop_1.7.6.9.zip
sudo unzip prestashop_1.7.6.9.zip  -d /var/www/html/prestashop/
sudo chown -R www-data: /var/www/html/prestashop/

#Configure prestashop website on port 80
sudo touch /etc/apache2/sites-available/prestashop.conf
cat > /etc/apache2/sites-available/prestashop.conf << EOM
<VirtualHost *:80>
    ServerAdmin ${website_admin}@${server_domain}
    ServerName ${server_domain}
    ServerAlias www.${server_domain}
    DocumentRoot /var/www/html/prestashop

    <Directory /var/www/html/prestashop>
        Options +FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /var/log/apache2/prestashop-error_log
    CustomLog /var/log/apache2/prestashop-access_log common
</VirtualHost>
EOM

#Enable prestashop website and remove apache2 default enabled site
sudo a2ensite prestashop.conf
sudo rm /etc/apache2/sites-enabled/000-default.conf
sudo systemctl restart apache2


#sudo rm -rf /var/www/html/prestashop/install/
#update ps_configuration set value="http://monddomain" where name='PS_SHOP_DOMAIN';
